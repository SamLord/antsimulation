# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Ant simulation in which black ants wander randomly until they find food and will then try and find a nest to drop the food at. They have short memories and have to remind each other of both nest and food locations.
In a similar way, red ants steal the food from black ants and then take the food to their own nests.

### What's in the repo? ###

* Visual Studio 2011 solution in C# using XNA

### Who do I talk to? ###

* Sam Lord - samlord6@gmail.com