﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Graphics;

namespace AntAssignment
{
    class Ant
    {

        private float locX;
        private float locY;
        private Vector2 location;
        
        private float maxSpeed;

        private float orientation;
        private Vector2 heading;

        private bool hasFood;
        private Vector2 foodLocation;
        private Vector2 nestLocation;

        private float sightRange;

        private Nest targetNest;
        private Food targetFood;

        protected Stopwatch foodMemSW = new Stopwatch();
        protected Stopwatch nestMemSW = new Stopwatch();

        private float memoryLength;

        private Texture2D antTexture;

        protected int debug = 0;


        /// <summary>
        /// Default constructor
        /// </summary>
        public Ant()
        {
            LocX = 0.0f;
            LocY = 0.0f;
            MaxSpeed = 1.0f;
            HasFood = false;
            FoodLocation = new Vector2(-1, -1);
            NestLocation = new Vector2(-1, -1);
            SightRange = 30.0f;
            memoryLength = 3000.0f;
            Orientation = 1.0f;
            Heading = OrientationToVector(Orientation);
        }

        /// <summary>
        /// Create a new Ant object
        /// </summary>
        /// <param name="x">'x' component of location</param>
        /// <param name="y">'y' component of location</param>
        /// <param name="speed">maximum speed of the ant</param>
        /// <param name="holdingFood">is the ant holding food?</param>
        /// <param name="viewRange">distance other ants must be within to communicate</param>
        /// <param name="memLength">how long the ant can remember information for (ms)</param>
        public Ant(float x, float y, float speed, bool holdingFood, float viewRange, float memLength)
        {
            LocX = x;
            LocY = y;
            MaxSpeed = speed;
            HasFood = holdingFood;
            FoodLocation = new Vector2(-1, -1);
            NestLocation = new Vector2(-1, -1);
            SightRange = viewRange;
            memoryLength = memLength;
            Orientation = 1.0f;
            Heading = OrientationToVector(Orientation);
        }

        /// <summary>
        /// Create a new Ant object
        /// </summary>
        /// <param name="x">'x' component of location</param>
        /// <param name="y">'y' component of location</param>
        /// <param name="speed">maximum speed of the ant</param>
        /// <param name="holdingFood">is the ant holding food?</param>
        /// <param name="viewRange">distance other ants must be within to communicate</param>
        /// <param name="memLength">how long the ant can remember information for (ms)</param>
        /// <param name="doDebug">do debug?</param>
        public Ant(float x, float y, float speed, bool holdingFood, float viewRange, float memLength, bool doDebug)
        {
            LocX = x;
            LocY = y;
            MaxSpeed = speed;
            HasFood = holdingFood;
            FoodLocation = new Vector2(-1, -1);
            NestLocation = new Vector2(-1, -1);
            SightRange = viewRange;
            memoryLength = memLength;
            Orientation = 1.0f;
            Heading = OrientationToVector(Orientation);
            if(doDebug){
                debug = 1;
            }
        }

        public Texture2D AntTexture
        {
            set
            {
                antTexture = value;
            }
            get
            {
                return antTexture;
            }
        }

        public Nest TargetNest
        {
            set
            {
                targetNest = value;
            }
            get
            {
                return targetNest;
            }
        }

        public Food TargetFood
        {
            set
            {
                targetFood = value;
            }
            get
            {
                return targetFood;
            }
        }


        public float SightRange
        {
            protected set
            {
                if (value < 0)
                {
                    sightRange = 5.0f;
                }
                else
                {
                    sightRange = value;
                }
            }
            get
            {
                return sightRange;
            }
        }

        public float LocX
        {   
            set
            {
                if (value < 0)
                {
                    locX = 0.0f;
                    location.X = 0.0f;
                }
                else
                {
                    locX = value;
                    location.X = value;
                }
            }
            get
            {
                return locX;
            }
        }

        public float LocY
        {
            set
            {
                if (value < 0)
                {
                    locY = 0.0f;
                    location.Y = 0.0f;
                }
                else
                {
                    locY = value;
                    location.Y = value;
                }
            }
            get
            {
                return locY;
            }
        }

        public Vector2 Location
        {
            set
            {
                location = value;
                LocX = value.X;
                locY = value.Y;
            }
            get
            {
                return location;
            }
        }

        protected float MaxSpeed
        {
            set
            {
                maxSpeed = value;
            }
            get
            {
                return maxSpeed;
            }
        }

        public bool HasFood
        {
            set
            {
                hasFood = value;
            }
            get
            {
                return hasFood;
            }
        }

        private Vector2 Heading
        {
            set
            {
                heading = value;
            }
            get
            {
                return heading;
            }
        }

        public float Orientation
        {
            set
            {

                if (value < 0)
                {
                    orientation = value + MathHelper.ToRadians(360); 
                }
                if (value > MathHelper.ToRadians(360))
                {
                    orientation = value - MathHelper.ToRadians(360);
                }
                else
                {
                    orientation = value;
                }

                if (Heading != OrientationToVector(Orientation))
                {
                    Heading = OrientationToVector(Orientation); //update the heading information with the new orientation
                }
            }
            get
            {
                return orientation;
            }
        }

        public Vector2 FoodLocation
        {
            protected set
            {
                if (value.X < 0 || value.Y < 0)
                {
                    foodLocation = new Vector2(-1,-1);
                }
                else
                {
                    foodLocation = value;
                }
            }
            get
            {
                return foodLocation;
            }
        }

        public Vector2 NestLocation
        {
            protected set
            {
                if (value.X < 0 || value.Y < 0)
                {
                    nestLocation = new Vector2(-1, -1);
                }
                else
                {
                    nestLocation = value;
                }
            }
            get
            {
                return nestLocation;
            }
        }

        protected float MemoryLength
        {
            set
            {
                memoryLength = value;
            }
            get
            {
                return memoryLength;
            }
        }

        /// <summary>
        /// Move the ant randomly
        /// </summary>
        protected void Wander()
        {
            Random theRandom = new Random(Guid.NewGuid().GetHashCode()); //new random

            int degOrientation = (int)MathHelper.ToRadians(theRandom.Next(-360, 361));

            if (degOrientation - 20 < degOrientation + 21)
            {
                Orientation += MathHelper.ToRadians((float)theRandom.Next(degOrientation - 20, degOrientation + 21)); //deviate from current heading by up to 20degs
            }
            else
            {
                Orientation += MathHelper.ToRadians((float)theRandom.Next(degOrientation + 20, degOrientation - 21)); //deviate from current heading by up to 20degs
            }
            Heading = OrientationToVector(Orientation); //update heading

            Location += Heading * (MaxSpeed); //move
        }

        /// <summary>
        /// Go towards a given location
        /// </summary>
        /// <param name="targetPos">Location to head towards</param>
        protected void GoTowards(Vector2 targetPos)
        {
            Heading = targetPos - Location; //calculate the direction as a vector

            Orientation = VectorToOrientation(Heading); //update orientation
            if (Heading != Vector2.Zero)
            {
                heading.Normalize();//normalise heading
            }

            Location += Heading * MaxSpeed; //move
        }

        /// <summary>
        /// Go towards a food object
        /// </summary>
        /// <param name="theFood">Food object to go towards</param>
        protected void GoTowards(Food theFood)
        {
            Heading = theFood.Location - Location;//calculate the direction as a vector

            Orientation = VectorToOrientation(Heading); //update orientation
            if (Heading != Vector2.Zero)
            {
                heading.Normalize();//normalise heading
            }

            Location += Heading * MaxSpeed;//move

        }

        /// <summary>
        /// Go towards a nest object
        /// </summary>
        /// <param name="theNest">nest to go towards</param>
        protected void GoTowards(Nest theNest)
        {
            Heading = theNest.Location - Location;//calculate the direction as vectio

            Orientation = VectorToOrientation(Heading);//update orientation
            if (Heading != Vector2.Zero)
            {
                heading.Normalize();//normalise heading
            }


            Location += Heading * MaxSpeed; //move

        }

        /// <summary>
        /// convert an orientation in radians to a heading
        /// </summary>
        /// <param name="deg">orientation to convert</param>
        /// <returns>Heading</returns>
        private Vector2 OrientationToVector(float deg)
        {
            Vector2 direction = new Vector2();

            direction.X = (float)Math.Cos(deg); //Cos of orientation get us our horizontal
            direction.Y = (float)Math.Sin(deg); //Sin gets us our vertical

            return direction;
        }
        
        /// <summary>
        /// Convert a vector heading to an orientation
        /// </summary>
        /// <param name="direction">heading vector</param>
        /// <returns>orientation</returns>
        private float VectorToOrientation(Vector2 direction)
        {
            float deg;

            if (direction != Vector2.Zero)
            {
                deg = (float)Math.Atan2(direction.Y, direction.X); //inverse tan of (Y,X) gives us the orientation
            }
            else
            {
                deg = 0;
            }
            return deg;
        }

        /// <summary>
        /// Ant 'brain'. If called every tick the ant will perform its behaviour.
        /// </summary>
        public void Live()
        {
            Vector2 minusOneVect = new Vector2(-1,-1);

            if (foodMemSW.ElapsedMilliseconds > memoryLength) //if we have known food location  for longer than our memory length...
            {
                ForgetFoodLocation(); //forget food location
            }
            if (nestMemSW.ElapsedMilliseconds > memoryLength)//if we have known nest location  for longer than our memory length...
            {
                ForgetNestLocation(); //forget nest location
            }

            if (debug > 0) // if debug flag is set...
            {
                Debug.WriteLine("Foodmem: {0}, Nestmem: {1}", foodMemSW.ElapsedMilliseconds, nestMemSW.ElapsedMilliseconds); //show the degbug info
            }

            if (HasFood && NestLocation != minusOneVect) //if ant has food and knows nest location...
            {
                GoTowards(NestLocation); //go towards nest
            }
            else if(!HasFood && FoodLocation != minusOneVect) //if ant has no food and knows food location...
            {
                GoTowards(FoodLocation); //go towards food location
                
                //calc distance to the food
                Vector2 difference = Location - FoodLocation;
                float distance = (float)Math.Sqrt((difference.X * difference.X) + (difference.Y * difference.Y));

                if (distance < SightRange && TargetFood.FoodPieces < 1)//if it's within sight range and there are no food peices left...
                {
                    ForgetFoodLocation();//forget the food was there
                    Wander();//wander
                }
            }
            else if (HasFood && NestLocation == minusOneVect) //if we have food and no nest location
            {
                Wander(); //wander
            }
            else //catchall
            {
                Wander(); //if in doubt wander
            }


        }

        /// <summary>
        /// Learn the location of a Food object
        /// </summary>
        /// <param name="theFood">food object to learn location of</param>
        public void LearnFoodLocation(Food theFood)
        {
            foodMemSW = Stopwatch.StartNew(); //start food memory timer
            FoodLocation = theFood.Location; //set the food location
            TargetFood = theFood; //set a reference to the food
        }

        /// <summary>
        /// Learn the location of a Nest object
        /// </summary>
        /// <param name="theNest">nest object to learn the location of</param>
        public void LearnNestLocation(Nest theNest)
        {
            nestMemSW = Stopwatch.StartNew(); //start nest memory timer
            NestLocation = theNest.Location; //learn nest location
            TargetNest = theNest; //set reference to the nest
        }

        /// <summary>
        /// Forget the location of the Food object
        /// </summary>
        public void ForgetFoodLocation()
        {
            foodMemSW.Reset(); //reset the timer to 0
            FoodLocation = new Vector2(-1, -1); //set flag so we know we don't have a food location
            TargetFood = null; //lose the reference to the food
        }

        /// <summary>
        /// Forget the location of the Nest object
        /// </summary>
        public void ForgetNestLocation()
        {
            nestMemSW.Reset(); //reset timer to very
            NestLocation = new Vector2(-1, -1); //set flag so we know we don't have a nest location
            TargetNest = null; //lose refernce to the nest
        }

        /// <summary>
        /// Rotate the ant
        /// </summary>
        /// <param name="amount">amount to rotate by</param>
        public void Rotate(float amount)
        {
            Orientation += amount;
            Heading = OrientationToVector(Orientation); //update heading to match
        }
    }
}
