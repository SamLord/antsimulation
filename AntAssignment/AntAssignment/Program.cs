using System;

namespace AntAssignment
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (AntProj game = new AntProj())
            {
                game.Run();
            }
        }
    }
#endif
}

