﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace AntAssignment
{
    class Food : StationaryGameObj
    {
        int foodPieces;
        Texture2D foodTexture;

        /// <summary>
        /// Create a new Food object with default values
        /// </summary>
        public Food(): base()
        {
            FoodPieces = 200;
            FoodTexture = null;
        }

        /// <summary>
        /// Create a new Food object
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="foodSize"></param>
        /// <param name="text"></param>
        public Food(int X, int Y, int foodSize, Texture2D text)
        {
            LocX = X;
            LocY = Y;
            FoodPieces = foodSize;
            foodTexture = text;
        }


        public int FoodPieces
        {
            set
            {
                foodPieces = value;
                Scale = foodPieces/5;
            }
            get
            {
                return foodPieces;
            }
        }

        public Texture2D FoodTexture
        {
            set
            {
                foodTexture = value;
            }
            get
            {
                return foodTexture;
            }
        }

        /// <summary>
        /// Remove pieces of food from the pile
        /// </summary>
        /// <param name="amount">amount to remove</param>
        public void TakeFood(int amount)
        {
            FoodPieces -= amount;
        }

        /// <summary>
        /// Remove a single piece of food from the pile
        /// </summary>
        public void TakeFood()
        {
            FoodPieces -= 1;
        }
    }
}
