﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace AntAssignment
{
    class Nest : StationaryGameObj
    {
        private Texture2D nestTexture;
        
        /// <summary>
        /// Create a new Nest object with default values
        /// </summary>
        public Nest() : base()
        {
            Scale = 1;
        }

        /// <summary>
        /// Create a new Nest object
        /// </summary>
        /// <param name="X">'x' component of location</param>
        /// <param name="Y">'y' component of location</param>
        /// <param name="nestScale">scale multiplier</param>
        /// <param name="text">texture of the nest</param>
        public Nest(float X, float Y, float nestScale, Texture2D text)
        {
            LocX = X;
            LocY = Y;
            Scale = nestScale;
            NestTexture = text;
            
        }

        

        public Texture2D NestTexture
        {
            set
            {
                nestTexture = value;
            }
            get
            {
                return nestTexture;
            }
        }

    }
}
