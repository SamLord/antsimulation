﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace AntAssignment
{
    class PredatorAnt : Ant
    {
        private WorkerAnt targetFood;

        /// <summary>
        /// Create a new PredatorAnt object with default values
        /// </summary>
        public PredatorAnt() : base()
        {

        }
        
        /// <summary>
        /// Create a new PredatorAnt object
        /// </summary>
        /// <param name="locationX">'x' component of location</param>
        /// <param name="locationY">'y' component of location</param>
        /// <param name="rot">rotation (radians)</param>
        /// <param name="antSpeed">maximum speed the ant can travel</param>
        /// <param name="hasFood">whether or not the ant currently has food</param>
        /// <param name="texture">the texture for the ant</param>
        /// <param name="memLen">how long the ant can remember food/nest locations (ms)</param>
        public PredatorAnt(float locationX, float locationY, float rot, float antSpeed, bool hasFood,Texture2D texture, float memLen)
        {
            LocX = locationX;
            LocY = locationY;
            MaxSpeed = antSpeed;
            HasFood = hasFood;

            NestLocation = new Vector2(-1, -1);

            AntTexture = texture;
            Orientation = rot;
            
        }

        /// <summary>
        /// Creatre a new PredatorAnt object
        /// </summary>
        /// <param name="locationX">'x' component of location</param>
        /// <param name="locationY">'y' component of location</param>
        /// <param name="rot">rotation (radians)</param>
        /// <param name="antSpeed">maximum speed the ant can travel</param>
        /// <param name="hasFood">whether or not the ant currently has food</param>
        /// <param name="texture">the texture for the ant</param>
        /// <param name="debugopt">Do debug? 1 = yes, 0 = no</param>
        public PredatorAnt(float locationX, float locationY, float rot, float antSpeed, bool hasFood, Texture2D texture, int debugopt)
        {
            LocX = locationX;
            LocY = locationY;
            MaxSpeed = antSpeed;
            HasFood = hasFood;

            NestLocation = new Vector2(-1, -1);

            AntTexture = texture;
            Orientation = rot;
            debug = debugopt;

            MemoryLength = 3000.0f;
        }

        public new WorkerAnt TargetFood
        {
            set
            {
                if (value != null)
                {
                    FoodLocation = value.Location;
                }
                targetFood = value;
            }
            get
            {
                return targetFood;
            }
        }

        /// <summary>
        /// Store a reference to our food source and the location we saw it
        /// </summary>
        /// <param name="theFood">The worker ant to register as food</param>
        private void LearnFoodLocation(WorkerAnt theFood)
        {
            foodMemSW = Stopwatch.StartNew(); //start the memory timer for the new food information
            FoodLocation = theFood.Location; //learn the location
            TargetFood = theFood; //save a reference to the food we're going to
        }

        /// <summary>
        /// Take food from a worker ant and change textures
        /// </summary>
        /// <param name="targetAnt">Ant to take food from</param>
        /// <param name="newTargetTexture">New texture for the worker ant</param>
        /// <param name="newPredatorTexture">New texture for the predator ant</param>
        public void TakeFood(WorkerAnt targetAnt, Texture2D newTargetTexture, Texture2D newPredatorTexture)
        {
            targetAnt.HasFood = false; //remove the food from the target
            targetAnt.AntTexture = newTargetTexture; //that ant has a texture reflecting its status
            HasFood = true; //this ant has food
            AntTexture = newPredatorTexture; // and a texture to match
        }

        /// <summary>
        /// Run the PredatorAnt brain logic
        /// </summary>
        public new void Live()
        {
            Vector2 minusOneVect = new Vector2(-1, -1);
            
            if (foodMemSW.ElapsedMilliseconds > MemoryLength) //if we've known about food longer than we can remember
            {
                ForgetFoodLocation(); //forget it
            }
            if (nestMemSW.ElapsedMilliseconds > MemoryLength)//if we've known about nest longer than we can remember
            {
                ForgetNestLocation(); //forget it
            }

            if (debug > 0)//if debug flag is set
            {
                Debug.WriteLine("Foodmem: {0}, Nestmem: {1}", foodMemSW.ElapsedMilliseconds, nestMemSW.ElapsedMilliseconds); //write information about the ant to debug
            }

            if (HasFood && NestLocation != minusOneVect) //if we have food and know where a nest is
            {
                GoTowards(NestLocation); //go towards the nest
            }
            else if (!HasFood && FoodLocation != minusOneVect) // if we don't have food and we know where food is
            {
                GoTowards(FoodLocation); //go towards the food location

                //work out the difference between us and the food location
                Vector2 difference = Location - FoodLocation; 
                float distance = (float)Math.Sqrt((difference.X * difference.X) + (difference.Y * difference.Y));

                if (distance < SightRange && !TargetFood.HasFood) //if we're close to where we're expecting food to be and our target doesn't have food
                {
                    ForgetFoodLocation(); //forget that target
                    TargetFood = null; //lose our reference to the target
                    Wander(); //wander randomly
                }
            }
            else if (HasFood && NestLocation == minusOneVect) //if we have food and we don't have a nest location
            {
                Wander(); //wander (i.e randomly search for one)
            }
            else //catchall
            {
                Wander(); //if in doubt, wander!
            }

        }

    }
}
