﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace AntAssignment
{
    class WorkerAnt : Ant
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public WorkerAnt() : base()
        {
        }

        /// <summary>
        /// Create a WorkerAnt object
        /// </summary>
        /// <param name="locationX">'x' component of location</param>
        /// <param name="locationY">'y' component of location</param>
        /// <param name="rot">roation</param>
        /// <param name="antSpeed">maximum speed ant can move</param>
        /// <param name="hasFood">does the ant have food?</param>
        /// <param name="texture">ant texture</param>
        public WorkerAnt(float locationX, float locationY, float rot, float antSpeed, bool hasFood,Texture2D texture)
        {
            LocX = locationX;
            LocY = locationY;
            MaxSpeed = antSpeed;
            HasFood = hasFood;

            NestLocation = new Vector2(-1, -1);

            AntTexture = texture;
            Orientation = rot;
            SightRange = 30.0f;
        }

        /// <summary>
        /// Create a WorkerAntObj
        /// </summary>
        /// <param name="locationX">'x' component of location</param>
        /// <param name="locationY">'y' component of location</param>
        /// <param name="rot">rotation</param>
        /// <param name="antSpeed">maximum speed the ant can move</param>
        /// <param name="hasFood">does the ant have food?</param>
        /// <param name="texture">ant texture</param>
        /// <param name="debugopt">Do debug? yes - 1, no - 0</param>
        public WorkerAnt(float locationX, float locationY, float rot, float antSpeed, bool hasFood, Texture2D texture, int debugopt)
        {
            LocX = locationX;
            LocY = locationY;
            MaxSpeed = antSpeed;
            HasFood = hasFood;

            NestLocation = new Vector2(-1, -1);

            AntTexture = texture;
            Orientation = rot;
            SightRange = 30.0f;
            debug = debugopt;
        }

        

        
    }
}
